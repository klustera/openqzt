echo "========================================"
echo "Infraestructura para ${PROJECT_NAME}"
echo "========================================"

## Creamos el nodo de consul
docker-machine create --driver google --google-project activegrillo --google-machine-type n1-standard-1 consul
docker-compose -f consul.yml --project-name ${PROJECT_NAME} up -d

## Obtenemos su dirección IP
cip=$(docker-machine ip consul)

## Swarm master
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --swarm-master --google-disk-size 100 --google-machine-type n1-standard-2 director

## Nodo para Flume
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376"  --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-disk-size 500 --google-machine-type n1-standard-8 ingesta-1

## Nodos para Computo
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376"  --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-disk-size 500 --google-machine-type n1-standard-8 computo-1

docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-disk-size 500 --google-machine-type n1-standard-8 computo-2

## Nodo para web
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-disk-size 100 --google-machine-type n1-standard-4 web-1

## Nodo para api
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-disk-size 100 --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-machine-type n1-standard-4 api-1

## Nodo para Cassandra
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-disk-size 1000  --google-machine-type n1-standard-4 cassandra-1

docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-disk-size 1000  --google-machine-type n1-standard-4 cassandra-2


## Nodo para Dashboard
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-disk-size 250 --google-machine-type n1-standard-4 dashboard-1

## Nodo para postgres
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376"  --google-disk-size 500 --google-machine-type n1-standard-4 postgres

## Nodo edge: Zeppelin, pyspark, jupyter, psql, etc
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376"  --google-scopes https://www.googleapis.com/auth/devstorage.read_write --google-disk-size 250 --google-machine-type n1-standard-4 edge

## Nodos para Zookeeper
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-disk-size 500 --google-machine-type n1-standard-4 zk-1

## Nodos para Kafka
docker-machine --debug create --driver google --google-project activegrillo --swarm --swarm-discovery="consul://$cip:8500" --engine-opt="cluster-store=consul://$cip:8500" --engine-opt="cluster-advertise=eth0:2376" --google-disk-size 500 --google-machine-type n1-standard-8 kafka-1


echo "Creando la red"
docker network create --driver overlay --subnet=10.0.9.0/24 ${PROJECT_NAME}-network (es importante el guión para Spark Streaming)

echo "Creando los servicios"

KAFKA_DOCKER_MACHINE_IP=$(docker-machine ip node-4) docker-compose --project-name ${PROJECT_NAME} stop

KAFKA_DOCKER_MACHINE_IP=$(docker-machine ip node-4) docker-compose --project-name ${PROJECT_NAME} rm --force

KAFKA_DOCKER_MACHINE_IP=$(docker-machine ip node-4) docker-compose --project-name ${PROJECT_NAME} build

KAFKA_DOCKER_MACHINE_IP=$(docker-machine ip node-4) docker-compose --project-name ${PROJECT_NAME} up -d flume zookeeper kafka
