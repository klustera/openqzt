dm_name=$1
apt_url='https://apt.dockerproject.org'
lsb_dist=$(docker-machine ssh ${dm_name} "lsb_release -a 2>/dev/null | grep Distributor | cut -f2 | tr '[:upper:]' '[:lower:]'")

dist_version=$(docker-machine ssh ${dm_name} "lsb_release -a 2>/dev/null | grep Codename | cut -f2")
arch=$(docker-machine ssh ${dm_name} dpkg --print-architecture)
repo='main'

docker-machine ssh ${dm_name} "echo deb [arch=${arch}] ${apt_url}/repo ${lsb_dist}-${dist_version} ${repo} | sudo tee /etc/apt/sources.list.d/docker.list"
docker-machine ssh ${dm_name} sudo apt-get update
docker-machine ssh ${dm_name} sudo apt-get install -y docker-engine
