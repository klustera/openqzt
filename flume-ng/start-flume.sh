#!/bin/bash

## Más memoria para Flume / Permitir que remotamente se pueda monitorear vía JMX
export JAVA_OPTS="-Xms100m -Xmx2000m -Dcom.sun.management.jmxremote"

for subdir in `ls ${HADOOP_HOME}/share/hadoop`; do
    FLUME_CLASSPATH="$FLUME_CLASSPATH:$HADOOP_HOME/share/hadoop/$subdir/hadoop-${subdir}-${HADOOP_VERSION}.jar"
done


FLUME_CLASSPATH="$FLUME_CLASSPATH:$HADOOP_HOME/share/hdfs/gcs-connector-latest-hadoop2.jar"

FLUME_CLASSPATH="$FLUME_CLASSPATH:$JAVA_HOME/lib/tools.jar"

FLUME_CONF_DIR=${FLUME_CONF_DIR:-/opt/flume/conf}

[[ -z "${FLUME_CONF_FILE}"  ]] && { echo "FLUME_CONF_FILE required";  exit 1; }
[[ -z "${FLUME_AGENT_NAME}" ]] && { echo "FLUME_AGENT_NAME required"; exit 1; }

echo "Starting flume agent : ${FLUME_AGENT_NAME}"

flume-ng agent \
         -Xms256m \
         -Xmx2000m \
         -c ${FLUME_CONF_DIR} \
         -f ${FLUME_CONF_FILE} \
         -n ${FLUME_AGENT_NAME} \
         -Dflume.root.logger=INFO,console
