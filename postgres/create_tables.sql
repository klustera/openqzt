-- Extensiones
create extension pgcrypto;
create extension fuzzystrmatch;
create extension hstore;
create extension postgres_fdw;
create extension tablefunc;
create extension cube;
create extension dict_xsyn;
create extension pg_trgm;
create extension "uuid-ossp";

-- Eliminar tablas
drop table if exists users;
drop table if exists clients;
drop table if exists branch_offices;
drop table if exists locations;
drop table if exists licenses;
drop table if exists crm_metadata;
drop table if exists crm_data;
drop table if exists facebook_data;
drop table if exists nodes;

-- Crear tablas
create table users (
       "user"  bigserial primary key,
       name varchar,
       client bigint,
       administrator boolean,
       phone varchar,
       email varchar,
       username varchar,
       password varchar,
       created_at timestamp default now(),
       updated_at timestamp default now()
);

create table clients (
       client bigserial primary key,
       name varchar,
       rfc varchar,
       address varchar,
       latitude float,
       longitude float,
       created_at timestamp default now(),
       updated_at timestamp default now()
);


create table branch_offices (
       branch_office bigserial primary key,
       client bigint,
       name varchar,
       address varchar,
       latitude float,
       longitude float,
       created_at timestamp default now(),
       updated_at timestamp default now()
);

create table locations (
       location bigserial primary key,
       client bigint,
       branch_office bigint,
       name varchar,
       created_at timestamp default now(),
       updated_at timestamp default now()
);

create table licenses (
       license bigserial primary key,
       client bigint,
       since timestamp,
       until timestamp,
       active boolean,
       created_at timestamp default now(),
       updated_at timestamp default now()
);

create table crm_metadata (
       crm_metadatum bigserial primary key,
       client bigint,
       description varchar,
       created_at timestamp default now(),
       updated_at timestamp default now()
);


-- ejemplo de insert:
-- INSERT INTO crm_data (crm_metadatum, device_mac, data)
-- VALUES (1, '00:aa:bb:cc',
-- '{"timestamp": "1423456.76", "cuestionario": [{"pregunta":"texto_pregunta_1", "respuesta":"texto_respuesta_1"}, {"pregunta":"texto_pregunta_2", "respuesta":"texto_respuesta_2"}]}');
create table crm_data (
       crm_datum bigserial primary key,
       crm_metadatum bigint,
       device_mac varchar,
       data JSONB,
       created_at timestamp default now(),
       updated_at timestamp default now()
);

create table facebook_data (
       facebook_datum bigserial primary key,
       device_mac varchar,
       data JSONB,
       -- el JSON contiene:
              -- fb_id varchar,
              -- email varchar,
              -- first_name varchar,
              -- middle_name varchar,
              -- last_name varchar,
              -- gender varchar,
              -- age int, -- (no es buena, trunca en 21)
              -- profile_link varchar,
              -- timezone varchar,
              -- fb_token varchar, -- (para preguntar datos offline)
              -- birthday date,
              -- likes varchar,  -- esta columna estaria vacia       
       created_at timestamp default now(),
       updated_at timestamp default now()
);

-- create table facebook_likes (
       -- los likes no se sacan al momento que se conecta la gente porque son muchos datos
       -- los saca manualmente despues
       -- los obtiene con otro algoritmo con el fb_token que permite sacar datos offline
       -- FB le da JSONs "paginados", cada uno apunta al siguiente link, pero el ultimo apunta al primero
              -- user varchar (otra vez),
              -- like varchar,
              -- categoria varchar,
              -- like_link varchar

       -- no se que pase si un usuario se conecta otra vez pero con mas likes.
--);

create table nodes (
       node bigserial primary key,
       mac_address varchar,
       client bigint,
       branch_office bigint,
       location bigint,
       installed_at date,
       removed_at date,
       created_at timestamp default now(),
       updated_at timestamp default now()
);


-- QUESTION: Podemos agregar tablas espaciales
