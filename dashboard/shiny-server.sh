#!/bin/sh

# Make sure the directory for individual app logs exists
mkdir -p /var/log/klustera-dashboard-server
chown -R shiny:shiny /var/log/klustera-dashboard-server

exec shiny-server >> /var/log/klustera-dashboard-server/klustera-dashboard-server.log 2>&1
